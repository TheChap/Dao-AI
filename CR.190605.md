# Rapport avancement projet IA

> Auteurs : **Antoine Chapusot, Mehdi Khelifa**  
> Année : **2019**  
> Cours : **Inteligence Artificielle**  
> Enseignante : **Mme Grau**  

## Compte-rendu 05 juin 2019

### Le DÀO

Le jeu de plateau s'appelle le ***Dào***. Il a été inventé en Chine au cours du XVIIIème siècle. Son nom signifie « *voie* » ou « *chemin* », mais aussi « *l’art de …* » . Il fait partie de la grande famille des jeux d’alignement de pions.

Le déplacement d’un pion s’effectue dans toutes les directions. Il faudra être le premier joueur à réaliser l’une des **3 combinaisons gagnantes** …  
*Il a été voté jeu de l'année en 2001 aux États-Unis par Mensa Select*

#### Les règles

Chaque joueur dispose ses **4 pierres** sur une diagonale.

|   1   |   .   |   .   |   2   |
| :---: | :---: | :---: | :---: |
|   .   |   1   |   2   |   .   |
|   .   |   2   |   1   |   .   |
|   2   |   .   |   .   |   1   |

Chacun, à son tour, déplace un de ses pions sur une ligne horizontale, verticale ou diagonale jusqu'à ce qu'il soit arrêté par un autre pion ou par le bord du plateau. On n'a pas le droit de sauter les pions.
L'objectif est de placer ses pions :

- soit sur une seule **ligne** horizontale ou verticale
- soit en formant un **carré** avec les pions adjacents
- soit en mettant un pion dans **chaque coin**.

Le premier joueur à réaliser une de ces combinaisons a gagné.

### Les défis que posent l'IA

#### La symétrie

Dào souffre d'un défaut: rien dans la règle n'impose la progression du jeu vers une fin de partie. Une partie peut donc **durer infiniment** tant qu'aucun joueur ne commet d'erreur.

De plus, toutes les combinaisons gagnantes sont valables **quelque soit l'orientation du plateau** de par le fait que le jeu n'a pas d'orientation imposée.

Ces deux points ci-dessus amènent à concevoir l'IA de sorte qu'elle puisse prendre en compte tous les cas de *rotations* mais aussi et surtout d'éviter au possible de se retrouver dans une **position non gagnante déjà rencontrée**.

Humainement, nous ne saisissons pas forcément qu'un positionnement de pions est semblable à un déjà rencontré si nous ne faisons pas pivoter le plateau. Mais l'IA va devoir en tenir compte pour être performante.

#### La fonction d'évaluation

Les règles du Dào en font un jeu très *simple* et très *équilibré*. Par conséquent, l'évaluation de l'état d'une partie à chaque instant est assez arbitrairement définie. Au cours du projet, **plusieurs fonctions d'évaluations** pourraient se succéder afin d'approcher au mieux du bon choix à fournir à l'algorithme.

A cela, il faut ajouter la difficulté d'évaluation selon les 3 structures gagnantes qui au final se déclinent en 6 positionnements possibles (***avec les rotations***) :

- Ligne à l'extremité du tableau

    |   .   |   .   |   .   |   .   |
    | :---: | :---: | :---: | :---: |
    |   .   |   .   |   .   |   .   |
    |   .   |   .   |   .   |   .   |
    |   1   |   1   |   1   |   1   |

- Ligne avant l'extremité du tableau

    |   .   |   .   |   .   |   .   |
    | :---: | :---: | :---: | :---: |
    |   .   |   .   |   .   |   .   |
    |   1   |   1   |   1   |   1   |
    |   .   |   .   |   .   |   .   |

- 1 pion aux 4 coins

    |   1   |   .   |   .   |   1   |
    | :---: | :---: | :---: | :---: |
    |   .   |   .   |   .   |   .   |
    |   .   |   .   |   .   |   .   |
    |   1   |   .   |   .   |   1   |

- Carré central

    |   .   |   .   |   .   |   .   |
    | :---: | :---: | :---: | :---: |
    |   .   |   1   |   1   |   .   |
    |   .   |   1   |   1   |   .   |
    |   .   |   .   |   .   |   .   |

- Carré bordure centré

    |   .   |   .   |   .   |   .   |
    | :---: | :---: | :---: | :---: |
    |   1   |   1   |   .   |   .   |
    |   1   |   1   |   .   |   .   |
    |   .   |   .   |   .   |   .   |

- Carré dans un coin

    |   .   |   .   |   .   |   .   |
    | :---: | :---: | :---: | :---: |
    |   .   |   .   |   .   |   .   |
    |   1   |   1   |   .   |   .   |
    |   1   |   1   |   .   |   .   |

Nous avons décidé de ne garder que les **combinaisons gagnantes** contenant les **carrés** donc 3.

### Nos solutions

Ce jeu de plateau se prête fortement à l'utilisation des algorithmes **min-max** puis à **alpha-beta**. Notre recherche va principalement se concentrer sur **l'évaluation fiable** d'un état de la partie. Afin de nous assurer de l'efficacité de notre IA, il va nous falloir adapter au fur et à mesure les fonctions d'évaluation.

### Evaluation du modèle

Chaque modèle se verra attribué **un score** correspondant au nombre de parties gagnantes *sur 100*, donc un pourcentage de réussite. Le nombres de parties jouées (*100*) pourra être modifié selon le temps d'execution.

Dans un premier temps, l'évaluation du modèle va se faire au regard de l'**aléatoire** : nous définissons l'adversaire de l'IA comme un joueur n'ayant aucune stratégie précise, donc jouant tous ses coups selon le hasard.

Une fois que le modèle aura dépassé l'aléatoire, nous pourrons le **versionner**. Nous construirons différents modèles basés sur différentes fonctions d'évaluations. Chaque modèle dépassant le précédent pourra être figé et s'ajoutera à la **liste des adversaires** constituée au départ du *RandomPlayer*

### Situation courante

Roadmap :

- [X] Méchaniques du jeu
- [X] Fonctions des joueurs
- [ ] Fonction d'évaluation (*en cours*)
- [ ] Modèle complet
- [ ] Entrainements
- [ ] Amélioration

Nous nous basons sur le fonctionnement de *aima-python* avec la class *Game*. Nous avons terminé les méchaniques de jeu et avons presque terminé l'inclusion de ce code dans la manière de fonctionner de *aima-python*. Nous avons esquisser une fonction d'évaluation. Sur chaque projeté de déplacement légal de chaque pion, elle se concentre sur les **carrés possibles** autour du pion déplacé et ajoute la **pondération** du nombre de pions aliés dans le carré.

### Sources

- [http://atoidejouhet.com/jeux-dasie-oceanie/dao/](http://atoidejouhet.com/jeux-dasie-oceanie/dao/)
- [https://www.trictrac.net/jeu-de-societe/dao](https://www.trictrac.net/jeu-de-societe/dao)
- [https://www.jeuxdenim.be/jeu-Dao](https://www.jeuxdenim.be/jeu-Dao)
- Code : [https://github.com/aimacode/aima-python](https://github.com/aimacode/aima-python)
