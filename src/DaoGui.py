import pygame
import os
import random

from pygame.locals import *
from src.Dao import *

class DaoGui:

    def __init__(self, *args, **kwargs):
        
        self.WINDOW_PARAMETERS = {
            'title': "DAO - MEK & CHAP @ ENSIIE 2019",
            'height':800,
            'width': 900        
        }
        self.WINDOW_PARAMETERS.update(kwargs)
        pygame.init()
        pygame.font.init()
        pygame.display.set_caption(self.WINDOW_PARAMETERS.get("title"))
        self.WHITE = (255, 255, 255)
        self.BLACK = (0, 0, 0)
        self.WINDOW_HEIGHT, self.WINDOW_WIDTH = self.WINDOW_PARAMETERS.get('height'), self.WINDOW_PARAMETERS.get('width')
        self.GRID_SIZE = 200
        self.GRID_COLOR = Color('Orange')
        self.infoZoneFont = pygame.font.SysFont('Comic Sans MS', 30)
        self.logZoneFont = pygame.font.SysFont('Comic Sans MS', 12)
        self.infoZoneTextsurface = self.infoZoneFont.render('Current player :', True, self.BLACK)
        self.icon_path = self.getIconSet()
        self.dao=Dao()
        self.window = pygame.display.set_mode((self.WINDOW_HEIGHT, self.WINDOW_WIDTH))
        self.src, self.dest = None, None  # Cellules source et destination pour déplacement
        self.pixelsMap,self.gridMap,self.infozone = None, None, None
        self.render(self.dao.initial)
        self.currentPlayer = self.dao.initial.to_move
        pygame.display.update()

    def render(self,state):
        '''
        refreshes the GUI and displays the new Grid
        resets src and dest
        '''
        self.pixelsMap = self.drawGrid()
        self.gridMap = self.syncGridToBoard(state)
        self.infozone = self.drawInfoZone()
        self.refreshInfoZone()
        self.src, self.dest = None, None  # Cellules source et destination pour déplacement

    def getIconSet(self):
        '''
        Charge le jeu d'icones aletoirement
        '''
        ressourceDir="src/ressources"
        imagesDir="{}/images".format(ressourceDir)
        themes = [x.name for x in os.scandir(
                    'src/ressources/images') if x.is_dir()]
        randomThme=themes[random.randint(0,len(themes)-1)]
        themeFiles = [x.path for x in os.scandir('{}/{}'.format(imagesDir,randomThme)) if x.is_file()]
        icon_path={
            'player0':"{}/available-circle.png".format(imagesDir)
        }
        icon_path.update(dict(zip(['player1','player2'],themeFiles)))
        return icon_path

    def drawGrid(self):
        '''
        Draw the grid with squares of self.GRID_SIZE px
        (Fills all the Window)
        returns the pixel map {cell:rect,start:coords,ends:coords}
        '''
        self.window.fill(Color("#A93D12"))
        pixelsMap = {}
        for i in range(0, self.WINDOW_HEIGHT, self.GRID_SIZE):
            for j in range(0, self.WINDOW_WIDTH-self.GRID_SIZE, self.GRID_SIZE):
                rect = pygame.draw.rect(
                    self.window, self.GRID_COLOR, Rect(i, j, self.GRID_SIZE, self.GRID_SIZE), 2)
                pixelsMap[(int(j/self.GRID_SIZE), int(i/self.GRID_SIZE))] = {"rect": rect,
                                                                 "starts": (i, j),
                                                                 "ends": (i+self.GRID_SIZE, j+self.GRID_SIZE)}
        return pixelsMap

    def drawInfoZone(self,thick=2):
        '''
        Dessine la zone d'info
        '''
        rect1 = pygame.draw.rect(self.window, self.GRID_COLOR, Rect(
            0, 800, self.GRID_SIZE*2, self.GRID_SIZE), thick)
        rect2 = pygame.draw.rect(self.window, self.GRID_COLOR, Rect(
            400, 800, self.GRID_SIZE*2, self.GRID_SIZE), thick)
        pygame.display.flip()
        return rect1, rect2

    def refreshInfoZone(self):
        '''
        Modifie le texte dans l'infozone
        '''
        infozone = self.drawInfoZone()
        self.window.blit(self.infoZoneTextsurface, (50, 825))
        current_player_pawn_image = pygame.image.load(
        self.icon_path['player{}'.format(self.currentPlayer)]).convert_alpha()
        self.window.blit(current_player_pawn_image, (275, 800))
        pygame.display.update()
    
    def refreshLogZone(self,text=""):
        '''
        Displays the text in logzone
        '''
        #print(len(text))
        self.window.fill(Color("#A93D12"), (400, 800,
                         self.GRID_SIZE*2, self.GRID_SIZE))
        rect2 = pygame.draw.rect(self.window, self.GRID_COLOR, Rect(
                400, 800, self.GRID_SIZE*2, self.GRID_SIZE), 2)
        
        pygame.display.flip()
        self.window.blit(self.logZoneFont.render(text,False,self.BLACK),(405,830))
        pygame.display.flip()


    def syncGridToBoard(self,state):
        '''
        lit le board (np.array 4x4) et place les images correspondantes sur la grille
        return un dict (cellule: {player:id_joueur,image_coords (coordonnées de l'image dans cette cellule)})
        '''
        innerGrid_offset = self.GRID_SIZE / 4
        board=state.board.getGrid()
        self.currentPlayer = state.to_move
        gridMap={}
        for i in range (0,len(board)):
            for j in range(0,len(board)):
                if board[j][i] == 1:
                    player1_pawn_image = pygame.image.load(self.icon_path['player1']).convert_alpha()          
                    self.window.blit(player1_pawn_image, (self.GRID_SIZE*i+innerGrid_offset,self.GRID_SIZE*j+innerGrid_offset))
                    gridMap[(j,i)]= {"player":1,
                                     "image":player1_pawn_image,
                                     "imgCoords":(int(self.GRID_SIZE*i+innerGrid_offset),int(self.GRID_SIZE*j+innerGrid_offset))
                    }
                elif board[j][i] == 2:
                    player2_pawn_image = pygame.image.load(self.icon_path['player2']).convert_alpha()          
                    self.window.blit(player2_pawn_image, (self.GRID_SIZE*i+innerGrid_offset,self.GRID_SIZE*j+innerGrid_offset))
                    gridMap[(j,i)]= {"player":2,
                                     "image":player2_pawn_image,
                                     "imgCoords":(int(self.GRID_SIZE*i+innerGrid_offset),int(self.GRID_SIZE*j+innerGrid_offset))
                    }
                else:
                    # minus 2 for the offset for the image to recover properly the cell
                    player0_pawn_image = pygame.image.load(self.icon_path['player0']).convert_alpha()
                    self.window.blit(player0_pawn_image, (self.GRID_SIZE*i +
                                                     innerGrid_offset-2, self.GRID_SIZE*j+innerGrid_offset-2))
                    gridMap[(j,i)]= {"player":0,
                                     "image": player0_pawn_image,
                                     "imgCoords":(int(self.GRID_SIZE*i+innerGrid_offset-2),int(self.GRID_SIZE*j+innerGrid_offset-2))
                    }
        pygame.display.flip()
        return gridMap

    def highLightSquare(self, x, y, color, width=0):
        '''
        Permet de colorier le contour d'une cell avec coin sup. gauche en (x,y)
        '''
        pygame.draw.rect(self.window, color, Rect(x, y, self.GRID_SIZE, self.GRID_SIZE), width)
        pygame.display.flip()

    def clickHandler(self,event,state):
        '''
        Gestion de l'event MOUSEBUTTONDOWN
        '''
        XClick, YClick = event.pos  # Recupération des coordonnées du clic
        self.refreshLogZone(str(self.getCellFromClick(event)[0]))
        if YClick > self.WINDOW_HEIGHT: return None
        if self.src is None : 
            self.src = self.getCellFromClick(event)
            if self.gridMap[self.src[0]]["player"] != self.currentPlayer: self.src = None ;return None
            if self.gridMap[self.src[0]]["player"] !=0 and self.gridMap[self.src[0]]["player"] == self.currentPlayer:
                self.highLightSquare(self.src[1]['starts'][0],self.src[1]['starts'][1],Color("blue"),2)
            return self.src[0]
        if self.src is not None and self.dest is None:
            self.dest = self.getCellFromClick(event)
            if self.src == self.dest:
                print("[ERROR] player {} > Must be different cells for src and dst".format(self.currentPlayer))
                self.refreshLogZone("[ERROR] > Must be different cells for src and dst")
                self.highLightSquare(self.src[1]['starts'][0],self.src[1]['starts'][1],self.GRID_COLOR,2)
                pygame.display.update()
                self.src,self.dest = None,None
                return None
            elif self.dest[0] not in state.board.getAllMoves(self.src[0]):        
                print("[ERROR] player {} > ({},{}) can only be moved in : [{}]".format(self.currentPlayer,
                                                    self.src[0][0],self.src[0][1],",".join([str(x) for x in state.board.getAllMoves(self.src[0])])))
                self.refreshLogZone("({},{}) possible moves : [{}]".format(
                                                    self.src[0][0],self.src[0][1],",".join([str(x) for x in state.board.getAllMoves(self.src[0])])))
                self.highLightSquare(self.src[1]['starts'][0],self.src[1]['starts'][1],self.GRID_COLOR,2)
                pygame.display.update()
                self.src,self.dest = None,None
                return None
            else:
                print("[INFO] player {} > Moving ({},{}) to ({},{})".format(self.currentPlayer,
                                                    self.src[0][0],self.src[0][1],self.dest[0][0],self.dest[0][1]))
                self.refreshLogZone("[INFO] > Moving ({},{}) to ({},{})".format(
                                                    self.src[0][0],self.src[0][1],self.dest[0][0],self.dest[0][1]))
                self.highLightSquare(
                    self.src[1]['starts'][0], self.src[1]['starts'][1], self.GRID_COLOR, 2)
                return self.dest[0]

    def getCellFromClick(self,event):
        '''
        Retourne la cellule en fonction du clic
        '''
        XClick, YClick = event.pos[0], event.pos[1]
        cellInfos = [
            (k, v) for k, v in self.pixelsMap.items()
            if XClick >= v['starts'][0] and XClick < v['ends'][0]
            and YClick >= v['starts'][1] and YClick < v['ends'][1]
        ][0]
        return cellInfos

    def gameOver(self,state):
        if self.dao.terminal_test(state):
            winner = 1 if state.to_move == 2 else 2
            self.refreshLogZone("Player {} WINS".format(winner))
            return True
        else: False
