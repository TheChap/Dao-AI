# -*- coding:utf8 -*-
from src.Game import Game
from src.Board import Board
from collections import namedtuple
from copy import deepcopy
GameState = namedtuple('GameState', 'to_move, utility, board, moves')


class Dao(Game):
    '''
    Classe princiaple du jeu DAO
    '''

    def __init__(self):
        board = Board()
        self.INFINITY = float('inf')
        self.pMax = 4
        self.initial = GameState(
            to_move=1, utility=0, board=board, moves=self.getPlayerMoves(board, 1))

    def actions(self, state):
        """Return a list of the allowable moves at this point."""
        return self.getPlayerMoves(state.to_move)

    def result(self, state, move):
        """Return the state that results from making a move from a state."""
        new_state = deepcopy(state)
        new_state.board.move(move)
        to_move = self.getOppositePlayer(state.to_move)
        moves = self.getPlayerMoves(new_state.board, to_move)
        utility = self.getUtility(state)
        new_state = GameState(to_move, utility, new_state.board, moves)
        return new_state

    def utility(self, state, player):
        """Return the value of this final state to player."""
        raise NotImplementedError

    def terminal_test(self, state):
        """Return True if this is a final state for the game."""
        return state.board.win_end()

    def to_move(self, state):
        """Return the player whose move it is in this state."""
        return state.to_move

    def display(self, state):
        """Print or otherwise display the state."""
        print("Joueur suivant :", state.to_move)
        print("Coups possibles =", state.moves)
        print("Grille de jeu")
        print(state.board.getGrid())

    def __repr__(self):
        return '<{}>'.format(self.__class__.__name__)

    def getPlayerMoves(self, board, player):
        '''
        return the dict of possible moves for player
        (key is player pawn coords: val is list of possible destinations)
        '''
        playerPawns = board.getPlayerPawns(player)
        moves_dict = dict()
        for pawn in playerPawns:
            moves_dict[pawn] = board.getAllMoves(pawn)
        return moves_dict

    def getOppositePlayer(self, x):
        '''
        Return the opposite player of x
        if 1 then 2 else 1
        '''
        if x == 1:
            return 2
        if x == 2:
            return 1
        raise IndexError

    def getUtility(self, state):
        '''
        evaluates the situation
        positive player : 1 | negative player : 2
        '''
        if state.board.win_end() == 1:
            return 10
        if state.board.win_end() == 2:
            return -10
        situation_player = [square.flatten().tolist().count(1) for square in state.board.getSquares()
                            if 2 not in square.flatten().tolist()]
        situation_opposite = [square.flatten().tolist().count(2) for square in state.board.getSquares()
                              if 1 not in square.flatten().tolist()]
        utility = len(situation_player) - len(situation_opposite)
        return utility

    def getUtilityWeighted(self, state):
        '''
        evaluates the situation
        positive player : 1 | negative player : 2
        '''
        if state.board.win_end() == 1:
            return 10
        if state.board.win_end() == 2:
            return -10
        situation_player = [square.flatten().tolist().count(1) for square in state.board.getSquares()
                            if 2 not in square.flatten().tolist()]
        situation_opposite = [square.flatten().tolist().count(2) for square in state.board.getSquares()
                              if 1 not in square.flatten().tolist()]
        utility = sum(situation_player) - sum(situation_opposite)
        return utility

    def getUtilityInvertedWeighted(self, state):
        '''
        evaluates the situation
        positive player : 2 | negative player : 1
        '''
        if state.board.win_end() == 2:
            return 10
        if state.board.win_end() == 1:
            return -10
        situation_player = [square.flatten().tolist().count(2) for square in state.board.getSquares()
                            if 1 not in square.flatten().tolist()]
        situation_opposite = [square.flatten().tolist().count(1) for square in state.board.getSquares()
                              if 2 not in square.flatten().tolist()]
        utility = sum(situation_player) - sum(situation_opposite)
        return utility

    def ai1(self, state):
        print()
        print("[INFO] ai1 joue")
        print("J'utilise la fonction d'évaluation qui compte les carrés favorables à une figure de victoire")
        return self.decisionMinMax(state, self.pMax, self.getUtility, False)

    def ai2_weighted(self, state):
        print()
        print("[INFO] ai2_weighted joue")
        print("J'utilise la fonction d'évaluation qui compte les carrés favorables à une figure de victoire \
et pondère par le nombre de pion des ces carrés")
        return self.decisionMinMax(state, self.pMax, self.getUtilityWeighted)

    def ai1_alphabeta(self, state):
        print()
        print("[INFO] ai1_alphabeta joue")
        print("J'utilise la fonction d'évaluation qui compte les carrés favorables à une figure de victoire")
        return self.decisionAlphaBeta(state, self.pMax, self.getUtility, False)

    def ai2_weighted_alphabeta(self, state):
        print()
        print("[INFO] ai2_weighted_alphabeta joue")
        print("J'utilise la fonction d'évaluation qui compte les carrés favorables à une figure de victoire \
et pondère par le nombre de pion des ces carrés")
        return self.decisionAlphaBeta(state, self.pMax, self.getUtilityWeighted)

    def ai3_inverted_weighted_alphabeta(self, state):
        print()
        print("[INFO] ai2_weighted_alphabeta joue")
        print("Je joue pour gêner l'adversaire")
        return self.decisionAlphaBeta(state, self.pMax, self.getUtilityInvertedWeighted)

    def randomPlayer(self, state):
        print()
        print("[INFO] randomPlayer joue")
        print("Je n'ai aucune stratégie :)")
        return state.board.randomMove(state.to_move)

    def play_game(self, *players):
        """Play an n-person, move-alternating game."""
        state = self.initial
        while True:
            for player in players:
                move = player(self, state)
                state = self.result(state, move)
                self.display(state)
                if self.terminal_test(state):
                    self.display(state)
                    winner = 1 if state.to_move == 2 else 2
                    return winner
                    # return self.getUtility(state, self.to_move(self.initial))

    def decisionMinMax(self, state, depth, getUtilityFunction, maximizingPlayer=True):
        '''
        Décide du meilleur 
        coup à jouer par le joueur J dans la situation e
        '''
        moveValues = {}
        moveList = [(src, dest) for src, destList in state.moves.items()
                    for dest in destList]
        for move in moveList:
            moveValues[move] = self.minimax(self.result(
                state, move), depth - 1, getUtilityFunction, maximizingPlayer)
        bestMove = max(moveValues, key=moveValues.__getitem__)
        return bestMove

    def decisionAlphaBeta(self, state, depth, getUtilityFunction, maximizingPlayer=True):
        '''
        Décide du meilleur 
        coup à jouer par le joueur J dans la situation e
        '''
        moveValues = {}
        moveList = [(src, dest) for src, destList in state.moves.items()
                    for dest in destList]
        for move in moveList:
            moveValues[move] = self.alphabeta(self.result(
                state, move), depth - 1, getUtilityFunction, -self.INFINITY, self.INFINITY, maximizingPlayer)
        bestMove = max(moveValues, key=moveValues.__getitem__)
        return bestMove

    def minimax(self, state, depth, getUtilityFunction, maximizingPlayer=False):
        if depth == 0 or state.board.win_end() is not False:
            return getUtilityFunction(state)

        if maximizingPlayer:
            maxEval = -self.INFINITY
            nextMoveList = [(src, dest) for src, destList in state.moves.items()
                            for dest in destList]
            for nextMove in nextMoveList:
                nextState = self.result(state, nextMove)
                eva = self.minimax(nextState, depth - 1,
                                   getUtilityFunction, False)
                maxEval = max(maxEval, eva)
            return maxEval

        else:
            minEval = self.INFINITY
            nextMoveList = [(src, dest) for src, destList in state.moves.items()
                            for dest in destList]
            for nextMove in nextMoveList:
                nextState = self.result(state, nextMove)
                eva = self.minimax(nextState, depth - 1,
                                   getUtilityFunction, True)
                minEval = min(minEval, eva)
            return minEval

    def alphabeta(self, state, depth, getUtilityFunction, alpha, beta, maximizingPlayer=False):
        if depth == 0 or state.board.win_end() is not False:
            return getUtilityFunction(state)

        if maximizingPlayer:
            maxEval = -self.INFINITY
            nextMoveList = [(src, dest) for src, destList in state.moves.items()
                            for dest in destList]
            for nextMove in nextMoveList:
                nextState = self.result(state, nextMove)
                eva = self.alphabeta(nextState, depth - 1,
                                     getUtilityFunction, alpha, beta, False)
                maxEval = max(maxEval, eva)
                alpha = max(alpha, eva)
                if beta <= alpha:
                    break
            return maxEval

        else:
            minEval = self.INFINITY
            nextMoveList = [(src, dest) for src, destList in state.moves.items()
                            for dest in destList]
            for nextMove in nextMoveList:
                nextState = self.result(state, nextMove)
                eva = self.alphabeta(nextState, depth - 1,
                                     getUtilityFunction, alpha, beta, True)
                minEval = min(minEval, eva)
                beta = min(beta, eva)
                if beta <= alpha:
                    break
            return minEval
