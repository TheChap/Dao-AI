#-*-coding:utf8-*-
import pygame
from pygame.locals import *
from Board import *
import numpy as np
import os,random


def loadIconSet():
    themes = [ x.name for x in os.scandir('src/images') if x.is_dir()]
    randomThme=themes[random.randint(0,len(themes)-1)]
    themeFiles = [x.path for x in os.scandir('src/images/{}'.format(randomThme)) if x.is_file()]
    icon_path={
        # #'player1':"src/images/classic-white-circle-pawn-100.png",
        # #'player2':"src/images/classic-black-circle-pawn-100.png",
        # 'player1':"src/images/iron-man-100.png",
        # 'player2':"src/images/thor-100.png",
        'player0':"src/images/available-circle.png"
    }
    icon_path.update(dict(zip(['player1','player2'],themeFiles)))
    return icon_path

def drawGrid(window,height,width,gridSize,color):
    '''
    Draw the grid with squares of gridSize px
    (Fills all the Window)
    returns the pixel map {cell:rect,start:coords,ends:coords}
    '''
    window.fill(Color("#A93D12"))
    pixelsMap={}
    for i in range(0,height,gridSize):
        for j in range(0,width-gridSize,gridSize):
            rect=pygame.draw.rect(window, color, Rect(i,j,gridSize,gridSize), 2)
            pixelsMap[(int(j/gridSize),int(i/gridSize))] = { "rect":rect,
                                                          "starts":(i,j),
                                                          "ends":(i+gridSize,j+gridSize)}
    return pixelsMap

def drawInfoZone(window,color,gridSize,width):
    rect1 = pygame.draw.rect(window, color, Rect(0, 800, gridSize*2, gridSize), width)
    rect2 = pygame.draw.rect(window, color, Rect(400, 800, gridSize*2, gridSize), width)
    pygame.display.flip()
    return rect1,rect2

def syncGridToBoard(window,board,gridSize,pixelsMap,icon_path):
    '''
    lit le board (np.array 4x4) et place les images correspondantes sur la grille
    return un dict (cellule: {player:id_joueur,image_coords (coordonnées de l'image dans cette cellule)})
    '''
    innerGrid_offset = gridSize / 4
    gridMap={}
    for i in range (0,len(board)):
        for j in range(0,len(board)):
            if board[j][i] == 1:
                player1_pawn_image = pygame.image.load(icon_path['player1']).convert_alpha()          
                window.blit(player1_pawn_image, (gridSize*i+innerGrid_offset,gridSize*j+innerGrid_offset))
                gridMap[(j,i)]= {"player":1,
                                 "image":player1_pawn_image,
                                 "imgCoords":(int(gridSize*i+innerGrid_offset),int(gridSize*j+innerGrid_offset))
                }
            elif board[j][i] == 2:
                player2_pawn_image = pygame.image.load(icon_path['player2']).convert_alpha()          
                window.blit(player2_pawn_image, (gridSize*i+innerGrid_offset,gridSize*j+innerGrid_offset))
                gridMap[(j,i)]= {"player":2,
                                 "image":player2_pawn_image,
                                 "imgCoords":(int(gridSize*i+innerGrid_offset),int(gridSize*j+innerGrid_offset))
                }
            else:
                # minus 2 for the offset for the image to recover properly the cell
                player0_pawn_image = pygame.image.load(icon_path['player0']).convert_alpha()
                window.blit(player0_pawn_image, (gridSize*i +
                                                 innerGrid_offset-2, gridSize*j+innerGrid_offset-2))
                gridMap[(j,i)]= {"player":0,
                                 "image": player0_pawn_image,
                                 "imgCoords":(int(gridSize*i+innerGrid_offset-2),int(gridSize*j+innerGrid_offset-2))
                }
    pygame.display.flip()
    return gridMap

def getCellFromClick(event,pixelsMap):
    '''
    Retourne la cellule en fonction du clic
    '''
    XClick,YClick=event.pos[0],event.pos[1]
    cellInfos = [
        (k,v) for k,v in pixelsMap.items() \
            if XClick >= v['starts'][0] and XClick < v['ends'][0] \
           and YClick >= v['starts'][1] and YClick < v['ends'][1] \
           ][0]
    return cellInfos



def highLightSquare(window,x,y,gridSize,color,width=0):
    '''
    Permet de colorier le contour d'une cell avec coin sup. gauche en (x,y)
    '''
    pygame.draw.rect(window, color, Rect(x,y,gridSize,gridSize), width)
    pygame.display.flip()


def main():
    pygame.init()
    pygame.font.init()
    myfont = pygame.font.SysFont('Comic Sans MS', 30)
    textsurface = myfont.render('Current player :', False, (0, 0, 0))
    pygame.display.set_caption("DAO - MEK & CHAP @ ENSIIE 2019")

    WHITE = (255, 255, 255)
    BLACK = (0, 0, 0)
    WINDOW_HEIGHT,WINDOW_WIDTH= 800,900
    GRID_SIZE=200
    GRID_COLOR = Color('Orange')
    icon_path = loadIconSet()

    window = pygame.display.set_mode((WINDOW_HEIGHT, WINDOW_WIDTH))
    board=Board()
    pixelsMap=drawGrid(window,WINDOW_HEIGHT,WINDOW_WIDTH,GRID_SIZE,GRID_COLOR)
    gridMap=syncGridToBoard(window,board.getGrid(),200,pixelsMap,icon_path)
    infozone=drawInfoZone(window,GRID_COLOR,GRID_SIZE,2)

    pygame.display.update()

    src,dest = None,None # Cellules source et destination pour déplacement
    continuer = True
    currentPlayer=1
    print("[INFO] CurrentPlayer is now player : {}".format(currentPlayer))
#BOUCLE INFINIE
    while continuer:
        infozone = drawInfoZone(window, GRID_COLOR, GRID_SIZE, 2)
        window.blit(textsurface, (50, 825))
        current_player_pawn_image = pygame.image.load(
        icon_path['player{}'.format(currentPlayer)]).convert_alpha()
        window.blit(current_player_pawn_image, (275, 800))
        pygame.display.update()

        pygame.time.Clock().tick(30) # Cadence l'affichage à 30 FPS
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                continuer = False
            if event.type == MOUSEBUTTONDOWN and event.button == 1:
                XClick,YClick=event.pos # Recupération des coordonnées du clic
                print(XClick,YClick)
                if YClick > WINDOW_HEIGHT: continue
                if src is None : 
                    src = getCellFromClick(event,pixelsMap)
                    if gridMap[src[0]]["player"] != currentPlayer: src = None ;continue
                    if gridMap[src[0]]["player"] !=0 and gridMap[src[0]]["player"] == currentPlayer:
                        highLightSquare(window,src[1]['starts'][0],src[1]['starts'][1],200,Color("blue"),2)
                    continue
                if dest is None:
                    dest = getCellFromClick(event,pixelsMap)
                    if src == dest:
                        print("[ERROR] player {} > Must have different cells for src and dst".format(currentPlayer))
                        highLightSquare(window,src[1]['starts'][0],src[1]['starts'][1],GRID_SIZE,GRID_COLOR,2)
                        pygame.display.update()
                        src,dest = None,None
                        continue
                    elif dest[0] not in board.getAllMoves(src[0]):
                        print("[ERROR] player {} > ({},{}) can only by moved in : [{}]".format(currentPlayer,
                                                            src[0][0],src[0][1],",".join([str(x) for x in board.getAllMoves(src[0])])))
                        highLightSquare(window,src[1]['starts'][0],src[1]['starts'][1],GRID_SIZE,GRID_COLOR,2)
                        pygame.display.update()
                        src,dest = None,None
                        continue
                    else:
                        print("[INFO] player {} > Moving ({},{}) to ({},{})".format(currentPlayer,
                                                            src[0][0],src[0][1],dest[0][0],dest[0][1]))
                        board.move((src[0],dest[0]))
                        pixelsMap=drawGrid(window,WINDOW_HEIGHT,WINDOW_WIDTH,GRID_SIZE,GRID_COLOR)
                        gridMap = syncGridToBoard(window, board.getGrid(), GRID_SIZE, pixelsMap,icon_path)
                        #print(board.getGrid())
                        pygame.display.update()
                        src,dest = None,None
                        if currentPlayer == 1: currentPlayer = 2
                        elif currentPlayer == 2: currentPlayer = 1
                        print("CurrentPlayer is now player {}:".format(currentPlayer))
                        continue

if __name__ == '__main__':
    main()
