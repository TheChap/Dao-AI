import numpy as np
import random


class Board:
    def __init__(self):
        '''
        Constructeur de Board
        '''
        self.grid = np.full((4, 4), 0, dtype=int)
        self.init_game()

    def init_game(self):
        """Initialize the game Board"""
        for i in range(4):
            self.grid[i, i] = 1
            self.grid[3-i, i] = 2
        return self

    def setGrid(self, grid):
        '''
        setter for Grid
        '''
        self.grid = grid
        #return self

    def getGrid(self):
        '''
        getter for Grid
        '''
        return self.grid

    def printGrid(self):
        """Prints grid"""
        print(self.grid)

    def isCellFree(self, cell):
        """True if cell is free"""
        return self.grid[cell] == 0

    def inRangeCell(self, cell):
        """True if cell is in the grid borders"""
        y, x = cell
        return x in range(0, 4) and y in range(0, 4)

    def getAllMoves(self, src):
        """Returns all legal moves from a src pawn"""
        src_y, src_x = src
        destinations = []
        for i in range(-1, 2):
            for j in range(-1, 2):
                if (i != 0 or j != 0):
                    cell_free = 0
                    y, x = src
                    y += i
                    x += j
                    while (self.inRangeCell((y, x)) and self.isCellFree((y, x))):
                        cell_free += 1
                        y += i
                        x += j
                    dest = (src_y+(i*cell_free), src_x+(j*cell_free))
                    if (dest != src):
                        destinations.append(dest)
        return destinations

    def move(self, move):
        """Set the dest value to src value and src value to 0 in the grid"""
        src, dest = move
        if self.isCellFree(dest):
            self.grid[dest] = self.grid[src]
            self.grid[src] = 0
        return self

    def rotate(self):
        """Rotate the grid 90' counter clockwise"""
        self.setGrid(np.rot90(self.grid))
        return self

    def win_end(self):
        """Return winner if grid contains a winning square, return False otherwise"""
        for square in self.getSquares():
            values = set(square.flatten().tolist())
            if len(values) == 1 and values != {0}:
                return values.pop()
        return False

    def getPlayerPawns(self, player):
        """Retreive the locations of all player's pawns"""
        locations = []
        for y in range(0, 4):
            for x in range(0, 4):
                if self.grid[(y, x)] == player:
                    locations.append((y, x))
        return locations

    def randomMove(self, player):
        """A player that chooses a legal move at random."""
        current_pawn = random.choice(self.getPlayerPawns(player))
        destinations = self.getAllMoves(current_pawn)
        while(destinations == []):
            current_pawn = random.choice(self.getPlayerPawns(player))
            destinations = self.getAllMoves(current_pawn)
        dest = random.choice(destinations)
        return (current_pawn, dest)

    def getSquares(self):
        '''
        get all squares of the current Grid
        '''
        result = []
        coords = [(0, 2), (1, 3), (2, 4)]

        for coordY in coords:
            for coordX in coords:
                result.append(
                    self.getGrid()[coordY[0]:coordY[1], coordX[0]:coordX[1]])
        return result
