#-*- coding:utf8 -*-
import pprint
pp = pprint.PrettyPrinter(indent=4)
'''
d={((0, 2), (0, 3)): 1, ((0, 2), (2, 0)): 3, ((0, 2), (3, 2)): 0, ((0, 2), (1, 3)): 0, 
    ((2, 1), (1, 1)): 1, ((2, 1), (0, 3)): 0, ((2, 1), (2, 0)): 1, ((2, 1), (2, 2)): -1, ((2, 1), (3, 2)): 2, 
    ((3, 0), (2, 0)): 4, 
    ((3, 1), (2, 0)): 3, ((3, 1), (1, 3)): -4, ((3, 1), (3, 3)): -4}
'''

from src.Board import *
from src.Dao import *
import graphviz

dao=Dao()
initBoard=np.array([[2, 2, 1, 0],
                    [2, 0, 0, 0],
                    [0, 1, 0, 2],
                    [1, 1, 0, 0]])

#dao.board.setGrid(initBoard)
board=Board()
board.setGrid(initBoard)
state = GameState(to_move=1,utility=0,board=board,moves=dao.getPlayerMoves(board,1))
#print(dao.minimax(state,3,True))
print(state.board.getGrid())
DEPTH=3
bestMove, moveValues = dao.decisionMinMax(state, 3, dao.getUtility)
nodes,edges=[],[]
bestState = dao.result(state,bestMove)
print(bestState.board.getGrid(),bestMove)
currentState=state
result={}

'''
def minimax(state, depth, maximizingPlayer=False):
    print(state.board.getGrid())
    if depth == 0 or state.board.win_end() is not False:
        return dao.getUtility(state)
    if maximizingPlayer:
        maxEval = -dao.INFINITY
        nextMoveList = [(src, dest) for src, destList in state.moves.items()
                        for dest in destList]
        for nextMove in nextMoveList:
            nextState = dao.result(state, nextMove)
            eva = minimax(nextState, depth - 1, False)
            maxEval = max(maxEval, eva)
            print("max(",maxEval,eva,") depth=",depth)
        return maxEval
    else:
        minEval = dao.INFINITY
        nextMoveList = [(src, dest) for src, destList in state.moves.items()
                        for dest in destList]
        for nextMove in nextMoveList:
            nextState = dao.result(state, nextMove)
            eva = minimax(nextState, depth - 1, True)
            minEval = min(minEval, eva)
            print("min(",minEval,eva,") depth=",depth)
        return minEval
'''

def minimaxTree(state,depth,tree={}):
    print("=================")
    if depth == 0 : return tree
    bestMove, moveValues = dao.decisionMinMax(state, depth,dao.getUtility)
    for move,values in moveValues.items():
        resultingState=dao.result(state, move)
        tree[str(move)] = {"board": [list(x) for x in resultingState.board.getGrid()],
                    #  'isBest':move == bestMove,
                      'depth':depth,'resultingState':resultingState}
    
    for k,v in tree.items():
        bMove, mVals = dao.decisionMinMax(
            v["resultingState"], depth-1, dao.getUtility)
        for m,val in mVals.items():
            rState=dao.result(v['resultingState'],m)
            tree[str(k)].update( {str(m) : {"board": [list(x) for x in rState.board.getGrid()],
                        #  'isBest': move == bestMove,
                          'depth': depth-1, 'resultingState': rState}}
                          )

    return tree

t = minimaxTree(state, 3)
pp.pprint(t)
