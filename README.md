# Dao-AI

## Context

+ IDE : VisualStudioCode
+ Language : Python 3.7.3

## Tests & launch

Un Makefile est à disposition pour le lancement des différents programmes

+ Pour lancer les tests :`make test`

+ Pour lancer le programme (temporaire): `python Board.py`

+ Pour lancer le jeu (Graphique) : `make run.gui`

## Fonctions du jeu

+ init_game(); créé une grille 4x4 avec les pions placés comme un début de partie :  

|   1   |   -   |   -   |   2   |
| :---: | :---: | :---: | :---: |
|   -   |   1   |   2   |   -   |
|   -   |   2   |   1   |   -   |
|   2   |   -   |   -   |   1   |

Dans ce module qui représente notre partie, nous avons un tuple nommé `GameState`  qui va stocké l'état du jeu pendant toute la durée de vie de la partie.

```python
GameState = namedtuple('GameState', 'to_move, utility, board, moves')
```

Les variables du GameState:

+ **to_move** : le joueur dont c'est le tour (1 ou 2)
+ **utility** : retour de la fonction d'évaluation de la situation pour le joueur
+ **board** : stocke l'état du plateau
+ **moves** : dict qui donne pour chaque pion du joueur en cours toutes les destinations possibles

Un move est un `tuple` comme suit : `(source, destination)` où `source` et `destination` sont également des `tuples` de coordonnées comme par exemple `(0, 2)`

+ `terminal_test(self, state)` ou `win_end()` : renvoie le joueur gagnant ou `False` si aucun gagnant
+ `getSquares(self)` : extrait les 9 carrés 2x2 possibles dans le plateau de 4x4
+ `getPlayerMoves(self,board, player)` : renvoie les coups possibles pour chaque pion du joueur
+ `getPlayerPawns(player)` : renvoie les positions des pions du joueur
+ `getAllMoves(self, src)` : renvoie toutes les destinations possibles d'un pion
+ `display(self, state)` : affiche l'état de la partie
+ `isCellFree(self, cell)` : indique si la cellule est libre pour accueillir un pion
+ `inRangeCell(self, cell)` : s'assure que les pions ne sortent pas du terrain
+ `move(self, move)` : effectue le déplacment du pion de source vers destination

## Fonctions de l'IA

+ `getUtility(self, state)` : fonction d'evaluation qui compte le **nombre de carré** libres pour le joueur pour y faire un carré gagnant
+ `getUtilityWeighted(self, state)` : même idée qu'au dessus mais en **pondérant** le compte avec le nombre de pions déjà présents
+ `minimax(self, state, depth, getUtilityFunction, maximizingPlayer=False)` : algorithme **`min-max`** appliqué à un état donné `state`, une profondeur maximum `depth`, une fonction d'évaluation choisie `getUtilityFunction` en précisant s'il faut maximiser le résultat ou non `maximizingPlayer` qui est à False par défaut
+ `alphabeta(self, state, depth, getUtilityFunction,alpha, beta, maximizingPlayer=False)` : algorithme **`alpha-beta`** similaire à l'algorithme **`min-max`** mais qui prend en compte les meilleurs valeures *minimales* et *maximales* rencontrés pour économiser du temps de calcul
+ `decisionMinMax(self,state, depth, getUtilityFunction, maximizingPlayer=True)` : renvoi le meilleur coup de l'algorithme **`min-max`**
+ `decisionAlphaBeta(self,state, depth, getUtilityFunction, maximizingPlayer=True)` : renvoi le meilleur coup de l'algorithme **`alpha-beta`**
+ `ai1(self,state)` : joue des coups selon l'algorithme **`min-max`** et la fonction d'evaluation `getUtility(self, state)`
+ `ai2_weighted(self,state)` : joue des coups selon l'algorithme **`min-max`** et la fonction d'evaluation `getUtilityWeighted(self, state)`
+ `ai1_alphabeta(self,state)` : joue des coups selon l'algorithme **`alpha-beta`** et la fonction d'evaluation `getUtility(self, state)`
+ `ai2_weighted_alphabeta(self,state)` : joue des coups selon l'algorithme **`alpha-beta`** et la fonction d'evaluation `getUtilityWeighted(self, state)`
+ `randomPlayer(self,state)` : joue tous ses coups aléatoirement
+ `randomMove(self, player)` : renvoie le déplacement aléatoire du joueur `randomPlayer(self,state)`
+ `result(self, state, move)` : projette le résultat d'un déplacement
