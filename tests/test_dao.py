import numpy as np
from src.Dao import *


def test_getPlayerOneMovesLengthIsFour():
    '''
    test that de dict as 4 keys
    '''
    party = Dao()
    playerOneMoves = party.getPlayerMoves(party.initial.board,1)
    assert len(playerOneMoves) == 4


def test_getPlayerOneMoves():
    '''
    test the integrity of the generated dict
    '''
    party = Dao()
    playerOneMoves = party.getPlayerMoves(party.initial.board,1)
    assert playerOneMoves == {(0, 0): [(0, 2), (2, 0)],
                              (1, 1): [(0, 1), (0, 2), (1, 0), (2, 0)],
                              (2, 2): [(1, 3), (2, 3), (3, 1), (3, 2)],
                              (3, 3): [(1, 3), (3, 1)]}

def test_getUtilityNotWeightedOnInitBoard():
    '''
    test the evalution function on initial board
    '''
    party = Dao()
    assert party.getUtility(party.initial) == 0


def test_getUtilityWeightedOnInitBoard():
    '''
    test the weighted evalution function on initial board
    '''
    party = Dao()
    assert party.getUtilityWeighted(party.initial) == 0


def test_getUtilityNotWeightedOnBoard():
    '''
    test the evalution function on initial board
    '''
    party = Dao()
    g = np.array([[0, 0, 0, 2],
                  [0, 1, 2, 0],
                  [1, 2, 1, 0],
                  [2, 0, 0, 1]])
    state = party.initial
    state.board.setGrid(g)
    gameState = GameState(board=state.board,to_move=1,
                          utility=0,moves=party.getPlayerMoves(state.board,1))
    assert party.getUtility(gameState) == 1


def test_getUtilityWeightedOnBoard():
    '''
    test the weighted evalution function on initial board
    '''
    party = Dao()
    g = np.array([[0, 0, 0, 2],
                  [0, 1, 2, 0],
                  [1, 2, 1, 0],
                  [2, 0, 0, 1]])
    state = party.initial
    state.board.setGrid(g)
    gameState = GameState(board=state.board, to_move=2,
                          utility=0, moves=party.getPlayerMoves(state.board,2))
    assert party.getUtility(gameState) == 1
