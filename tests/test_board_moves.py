import numpy as np
from src.Board import *

# horizontals


def test_getAllMoves_horizontals_no_left():
    b = Board()
    b.setGrid(np.array([[2, 0, 0, 1],
                                  [2, 2, 1, 2],
                                  [2, 1, 2, 2],
                                  [1, 1, 1, 2]]))
    src = (0, 0)
    assert b.getAllMoves(src) == [(0, 2)]


def test_getAllMoves_horizontals_no_right():
    b = Board()
    b.setGrid(np.array([[2, 0, 0, 1],
                                  [2, 2, 1, 2],
                                  [2, 1, 2, 2],
                                  [2, 0, 0, 1]]))
    src = (3, 3)
    assert b.getAllMoves(src) == [(3, 1)]


def test_getAllMoves_horizontals():
    b = Board()
    b.setGrid(np.array([[2, 1, 2, 2],
                                  [0, 1, 0, 0],
                                  [2, 1, 2, 2],
                                  [1, 1, 1, 2]]))
    src = (1, 1)
    assert b.getAllMoves(src) == [(1, 0), (1, 3)]


def test_getAllMoves_horizontals2():
    b = Board()
    b.setGrid(np.array([[2, 1, 2, 2],
                                  [0, 1, 1, 0],
                                  [2, 1, 2, 2],
                                  [1, 1, 1, 2]]))
    src = (1, 1)
    assert b.getAllMoves(src) == [(1, 0)]

# Verticals


def test_getAllMoves_verticals_no_up():
    b = Board()
    b.setGrid(np.array([[2, 1, 1, 1],
                                  [2, 2, 0, 2],
                                  [2, 1, 0, 2],
                                  [1, 1, 1, 2]]))
    src = (0, 2)
    assert b.getAllMoves(src) == [(2, 2)]


def test_getAllMoves_verticals_no_down():
    b = Board()
    b.setGrid(np.array([[2, 0, 0, 0],
                                  [2, 2, 1, 0],
                                  [2, 1, 2, 0],
                                  [2, 0, 1, 1]]))
    src = (3, 3)
    assert b.getAllMoves(src) == [(0, 3)]


def test_getAllMoves_verticals():
    b = Board()
    b.setGrid(np.array([[2, 0, 2, 2],
                                  [1, 1, 1, 1],
                                  [2, 0, 2, 2],
                                  [1, 0, 1, 2]]))
    src = (1, 1)
    assert b.getAllMoves(src) == [(0, 1), (3, 1)]


# Diagonales

def test_getAllMoves_diag_up_left():
    b = Board()
    b.setGrid(np.array([[0, 1, 1, 1],
                                  [2, 0, 1, 2],
                                  [2, 1, 1, 2],
                                  [1, 1, 1, 2]]))
    src = (2, 2)
    assert b.getAllMoves(src) == [(0, 0)]


def test_getAllMoves_diag_up_right():
    b = Board()
    b.setGrid(np.array([[1, 0, 1, 0],
                                  [1, 1, 0, 1],
                                  [1, 1, 1, 1],
                                  [1, 1, 1, 1]]))
    src = (2, 1)
    assert b.getAllMoves(src) == [(0, 3)]


def test_getAllMoves_diag_down_left():
    b = Board()
    b.setGrid(np.array([[1, 1, 1, 1],
                                  [1, 1, 0, 1],
                                  [1, 0, 1, 1],
                                  [1, 1, 1, 1]]))
    src = (0, 3)
    assert b.getAllMoves(src) == [(2, 1)]


def test_getAllMoves_diag_down_right():
    b = Board()
    b.setGrid(np.array([[1, 1, 1, 1],
                                  [1, 1, 0, 1],
                                  [1, 0, 1, 1],
                                  [1, 1, 1, 1]]))
    src = (0, 1)
    assert b.getAllMoves(src) == [(1, 2)]


def test_getAllMoves_diag_multi():
    b = Board()
    b.setGrid(np.array([[1, 1, 1, 0],
                                  [0, 1, 0, 1],
                                  [1, 1, 1, 1],
                                  [0, 1, 0, 1]]))
    src = (2, 1)
    assert b.getAllMoves(src) == [(1, 0), (0, 3), (3, 0), (3, 2)]
