import numpy as np
from src.Board import *


def test_getPlayerPawns_p1():
    b = Board()
    assert b.getPlayerPawns(1) == [(0, 0), (1, 1), (2, 2), (3, 3)]


def test_getPlayerPawns_p2():
    b = Board()
    assert b.getPlayerPawns(2) == [(0, 3), (1, 2), (2, 1), (3, 0)]
