import numpy as np
from src.Board import *


def test_new_Board():
    b = Board()
    assert np.array_equal(b.grid, np.array([[1, 0, 0, 2],
                                            [0, 1, 2, 0],
                                            [0, 2, 1, 0],
                                            [2, 0, 0, 1]]))


def test_Board_rotate():
    b = Board()
    b.rotate()
    assert np.array_equal(b.grid, np.array([[2, 0, 0, 1],
                                            [0, 2, 1, 0],
                                            [0, 1, 2, 0],
                                            [1, 0, 0, 2]]))


def test_isCellFree():
    b = Board()
    dest = (1, 0)
    assert b.isCellFree(dest) == True


def test_isCellNotFree():
    b = Board()
    dest = (0, 0)
    assert b.isCellFree(dest) == False


def test_inRangeCell():
    b = Board()
    dest = (0, 0)
    assert b.inRangeCell(dest) == True


def test_inRangeCell_1():
    b = Board()
    dest = (3, 3)
    assert b.inRangeCell(dest) == True


def test_not_inRangeCell():
    b = Board()
    dest = (-1, 0)
    assert b.inRangeCell(dest) == False


def test_not_inRangeCell_1():
    b = Board()
    dest = (0, -1)
    assert b.inRangeCell(dest) == False


def test_not_inRangeCell_2():
    b = Board()
    dest = (-1, -1)
    assert b.inRangeCell(dest) == False


def test_Board_move():
    b = Board()
    src = (0, 0)
    dest = (2, 0)
    b.move((src, dest))
    assert np.array_equal(b.grid, np.array([[0, 0, 0, 2],
                                            [0, 1, 2, 0],
                                            [1, 2, 1, 0],
                                            [2, 0, 0, 1]]))


def test_Board_getSquaresLenIsNine():
    '''
    Test squares List Length (Must be equal to 0)
    '''
    b = Board()
    result = b.getSquares()
    assert len(result) == 9


def test_Board_getSquares():
    '''
    Test squares List Integrity
    '''
    b = Board()
    result = b.getSquares()
    assert len(result) == 9

    assert np.array_equal(result,[np.array([[1, 0],
                                [0, 1]]),
                      np.array([[0, 0],
                                [1, 2]]),
                      np.array([[0, 2],
                                [2, 0]]),
                      np.array([[0, 1],
                                [0, 2]]),
                      np.array([[1, 2],
                                [2, 1]]),
                      np.array([[2, 0],
                                [1, 0]]),
                      np.array([[0, 2],
                                [2, 0]]),
                      np.array([[2, 1],
                                [0, 0]]),
                      np.array([[1, 0],
                                [0, 1]])
                      ]
    )