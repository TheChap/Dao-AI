import numpy as np
from src.Board import *

# Wins

# Not using other figures than squares
# # Fig exterieur
# def test_win_exte():
#     g = np.array([[1, 0, 0, 1],
#                   [0, 0, 0, 0],
#                   [0, 0, 0, 0],
#                   [1, 0, 0, 1]])
#     b = Board()
#     b.setGrid(g)
#     assert b.win_end() == True

# # Verticals


# def test_win_vertical_1():
#     g = np.array([[1, 0, 0, 0],
#                   [1, 0, 0, 0],
#                   [1, 0, 0, 0],
#                   [1, 0, 0, 0]])
#     b = Board()
#     b.setGrid(g)
#     assert b.win_end() == True


# def test_win_vertical_2():
#     g = np.array([[0, 1, 0, 0],
#                   [0, 1, 0, 0],
#                   [0, 1, 0, 0],
#                   [0, 1, 0, 0]])
#     b = Board()
#     b.setGrid(g)
#     assert b.win_end() == True


# def test_win_vertical_3():
#     g = np.array([[0, 0, 1, 0],
#                   [0, 0, 1, 0],
#                   [0, 0, 1, 0],
#                   [0, 0, 1, 0]])
#     b = Board()
#     b.setGrid(g)
#     assert b.win_end() == True


# def test_win_vertical_4():
#     g = np.array([[0, 0, 0, 1],
#                   [0, 0, 0, 1],
#                   [0, 0, 0, 1],
#                   [0, 0, 0, 1]])
#     b = Board()
#     b.setGrid(g)
#     assert b.win_end() == True

# # Horizontals


# def test_win_horizontal_1():
#     g = np.array([[1, 1, 1, 1],
#                   [0, 0, 0, 0],
#                   [0, 0, 0, 0],
#                   [0, 0, 0, 0]])
#     b = Board()
#     b.setGrid(g)
#     assert b.win_end() == True


# def test_win_horizontal_2():
#     g = np.array([[0, 0, 0, 0],
#                   [1, 1, 1, 1],
#                   [0, 0, 0, 0],
#                   [0, 0, 0, 0]])
#     b = Board()
#     b.setGrid(g)
#     assert b.win_end() == True


# def test_win_horizontal_3():
#     g = np.array([[0, 0, 0, 0],
#                   [0, 0, 0, 0],
#                   [1, 1, 1, 1],
#                   [0, 0, 0, 0]])
#     b = Board()
#     b.setGrid(g)
#     assert b.win_end() == True


# def test_win_horizontal_4():
#     g = np.array([[0, 0, 0, 0],
#                   [0, 0, 0, 0],
#                   [0, 0, 0, 0],
#                   [1, 1, 1, 1]])
#     b = Board()
#     b.setGrid(g)
#     assert b.win_end() == True

# Squares


def test_win_centered_square():
    g = np.array([[0, 0, 0, 0],
                  [0, 1, 1, 0],
                  [0, 1, 1, 0],
                  [0, 0, 0, 0]])
    b = Board()
    b.setGrid(g)
    assert b.win_end() == 1


def test_win_bottom_left_square():
    g = np.array([[0, 0, 0, 0],
                  [0, 0, 0, 0],
                  [1, 1, 0, 0],
                  [1, 1, 0, 0]])
    b = Board()
    b.setGrid(g)
    assert b.win_end() == 1


def test_win_bottom_right_square():
    g = np.array([[0, 0, 0, 0],
                  [0, 0, 0, 0],
                  [0, 0, 1, 1],
                  [0, 0, 1, 1]])
    b = Board()
    b.setGrid(g)
    assert b.win_end() == 1


def test_win_upper_left_square():
    g = np.array([[1, 1, 0, 0],
                  [1, 1, 0, 0],
                  [0, 0, 0, 0],
                  [0, 0, 0, 0]])
    b = Board()
    b.setGrid(g)
    assert b.win_end() == 1


def test_win_upper_right_square():
    g = np.array([[0, 0, 1, 1],
                  [0, 0, 1, 1],
                  [0, 0, 0, 0],
                  [0, 0, 0, 0]])
    b = Board()
    b.setGrid(g)
    assert b.win_end() == 1


def test_win_upper_center_square():
    g = np.array([[0, 1, 1, 0],
                  [0, 1, 1, 0],
                  [0, 0, 0, 0],
                  [0, 0, 0, 0]])
    b = Board()
    b.setGrid(g)
    assert b.win_end() == 1


def test_win_bottom_center_square():
    g = np.array([[0, 0, 0, 0],
                  [0, 0, 0, 0],
                  [0, 1, 1, 0],
                  [0, 1, 1, 0]])
    b = Board()
    b.setGrid(g)
    assert b.win_end() == 1


def test_win_left_center_square():
    g = np.array([[0, 0, 0, 0],
                  [2, 2, 0, 0],
                  [2, 2, 0, 0],
                  [0, 0, 0, 0]])
    b = Board()
    b.setGrid(g)
    assert b.win_end() == 2


def test_win_right_center_square():
    g = np.array([[0, 0, 0, 0],
                  [0, 0, 1, 1],
                  [0, 0, 1, 1],
                  [0, 0, 0, 0]])
    b = Board()
    b.setGrid(g)
    assert b.win_end() == 1

# Failures

def test_not_win_1():
    g = np.array([[0, 0, 0, 0],
                  [0, 0, 1, 1],
                  [0, 0, 0, 1],
                  [0, 0, 0, 0]])
    b = Board()
    b.setGrid(g)
    assert b.win_end() == False

def test_not_win_2():
    g = np.array([[0, 2, 2, 0],
                  [0, 0, 2, 0],
                  [0, 2, 0, 2],
                  [0, 0, 2, 0]])
    b = Board()
    b.setGrid(g)
    assert b.win_end() == False

def test_not_win_init():
    b = Board()
    assert b.win_end() == False

def test_not_win_3():
    g = np.array([[2, 0, 0, 1],
                  [2, 0, 0, 1],
                  [2, 0, 0, 1],
                  [2, 0, 0, 1]])
    b = Board()
    b.setGrid(g)
    assert b.win_end() == False