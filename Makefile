all: run.gui

test:
	python3 -m pytest
run.sample:
	python3 Game_samples.py

run.gui: gui.reqs
	python3 main.py

gui.reqs:
	pip3 install pygame==1.9.6

clean:
	find . -iname "*pyc" | xargs rm -v