#-*- coding:utf8 -*-
from src.DaoGui import *
from pygame.locals import *

def quitGame():
    pygame.quit()
    quit()

def mainMenu():
    '''
    https://www.sourcecodester.com/tutorials/python/11784/python-pygame-simple-main-menu-selection.html
    '''
    # Game Initialization
    pygame.init()
    # Center the Game Application
    os.environ['SDL_VIDEO_CENTERED'] = '1'
    # Game Resolution
    screen_width = 800
    screen_height = 900
    screen = pygame.display.set_mode((screen_width, screen_height))
    # Text Renderer
    def text_format(message, textFont, textSize, textColor):
        newFont = pygame.font.Font(textFont, textSize)
        newText = newFont.render(message, 0, textColor)
        return newText
    # Colors
    white = (255, 255, 255)
    black = (0, 0, 0)
    gray = (50, 50, 50)
    red = (255, 0, 0)
    green = (0, 255, 0)
    blue = (0, 0, 255)
    yellow = (255, 255, 0)

    # Game Fonts
    font = "src/ressources/fonts/Retro.ttf"

    # Game Framerate
    clock = pygame.time.Clock()
    FPS = 30
    menu = True
    menuItems=[{
        "label": "Play One vs One",
        "selected": True,
        "textObj": None
        },
        {
        "label": "Play One vs IA [EASY]",
        "selected": False,
        "textObj": None
        },
        {
        "label": "Play One vs IA [MEDIUM]",
        "selected": False,
        "textObj": None
        },
        {
        "label": "Play One vs IA [HARD]",
        "selected": False,
        "textObj": None
        },
        {
        "label": "Play One vs IA [HARD2]",
        "selected": False,
        "textObj": None
        },
        {
        "label": "Exit",
        "selected": False,
        "textObj": None
        },
        
    ]
    itemList = [x['label'] for x in menuItems]
    itemIndex =0
    selected = [x['label'] for x in menuItems if x['selected']][0]
    while menu:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                quitGame()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_UP:
                    itemIndex -=1
                    selected = itemList[itemIndex % len(itemList)]
                elif event.key == pygame.K_DOWN:
                    itemIndex +=1
                    selected = itemList[itemIndex % len(itemList)]
                if event.key == pygame.K_RETURN:
                    if selected == "Play One vs One":
                        play(None,selected)
                    if selected == "Play One vs IA [EASY]":
                        play(Dao().randomPlayer, selected)
                    if selected == "Play One vs IA [MEDIUM]":
                        play(Dao().ai1_alphabeta,selected)
                    if selected == "Play One vs IA [HARD]":
                        play(Dao().ai2_weighted_alphabeta,selected)
                    if selected == "Play One vs IA [HARD2]":
                        play(Dao().ai3_inverted_weighted_alphabeta, selected)
                    if selected == "Exit":
                        quitGame()

        # Main Menu UI
        screen.fill(blue)
        title = text_format("DAO - MEK & CHAP @ ENSIIE 2019", font, 75, yellow)
        for menuItem in menuItems:
            if selected == menuItem['label']:
                menuItem['textObj'] = text_format(
                    menuItem['label'], font, 75, white)
            else:
                menuItem['textObj'] = text_format(
                    menuItem['label'], font, 75, black)

        # Main Menu Text
        title_rect = title.get_rect()
        screen.blit(title, (screen_width/2 - (title_rect[2]/2), 80))
 
        # Menu items display
        textObjectList =[x['textObj'] for x in menuItems if x is not None]
        for i,textObject in enumerate(textObjectList):
            text_rect = textObject.get_rect()
            screen.blit(textObject, (screen_width/2 - (text_rect[2]/2), 300+i*60))
        
        pygame.display.update()
        clock.tick(FPS)
        pygame.display.set_caption(
            "DAO - MEK & CHAP @ ENSIIE 2019 MAIN MENU")         # Add exit function


def play(challenger=None,modeInfo=None):

    guiTitle = "DAO - MEK & CHAP @ ENSIIE 2019 | {}".format(modeInfo)
    
    daoGui = DaoGui(title=guiTitle)
    state=daoGui.dao.initial
    continuer = True
    while continuer:
        pygame.time.Clock().tick(30) # Cadence l'affichage à 30 FPS 
        if challenger is None or state.to_move == 1:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    continuer = False
                if event.type == MOUSEBUTTONDOWN and event.button == 1:
                    daoGui.clickHandler(event,state)
                    if daoGui.src is not None and daoGui.dest is not None:
                        move = (daoGui.src[0],daoGui.dest[0])
                        state=daoGui.dao.result(state,move)
                        daoGui.render(state)
                        if daoGui.gameOver(state):
                            continuer = False
        #elif challenger is None: continue
        elif challenger is not None and state.to_move == 2:
            move = challenger(state)
            state = daoGui.dao.result(state, move)
            daoGui.render(state)
            if daoGui.gameOver(state):
                continuer = False



if __name__ == "__main__":
    mainMenu()
