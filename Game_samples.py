from src.Board import *
from src.Dao import *
import numpy as np


def random_move():
    print("Initialized Board")
    board = Board()
    board.printGrid()
    print("==========")
    print("Random Move Player 1")
    board.randomMove(1)
    board.printGrid()
    print("==========")


def full_random():
    board = Board()
    for i in range(51):
        board.randomMove(1)
        board.randomMove(2)
    print("Random Move Player 1 & 2 : 50 turns")
    board.printGrid()
    print("==========")


def one_move():
    print("1 step")
    current_play = Dao()
    state = current_play.initial
    print(state)
    move = current_play.randomPlayer(state)
    print(move)
    state = current_play.result(state, move)
    print(state)


def result_try():
    print("1 move specified, testing")
    current_play = Dao()
    state = current_play.initial
    print(state.board.getGrid())
    print(state)
    move = ((0, 0), (2, 0))
    state = current_play.result(state, move)
    print(state.board.getGrid())
    print(state)

def manual_minmax():
    d = Dao()
    board = Board()
    board.setGrid(np.array([[2, 2, 1, 0],
                            [2, 0, 0, 0],
                            [0, 1, 0, 2],
                            [1, 1, 0, 0]]))
    state = GameState(to_move=1, utility=0, board=board,
                      moves=d.getPlayerMoves(board, 1))
    moveList = [(src, dest) for src, destList in state.moves.items()
                for dest in destList]
    print(moveList)
    for move in moveList:
        print(move)
        print(d.result(state, move).board.getGrid())

    print(d.minimax(state, 3,d.getUtility, True))
    print(d.decisionMinMax(state, 3,d.getUtility, True))

def one_try():
    print("== 1 game try ==")
    current_play = Dao()
    current_play.play_game(Dao.ai1, Dao.randomPlayer)

def multi_game():
    print()
    print("== multi game try MIN-MAX==")
    print()
    current_play = Dao()
    IA = Dao.ai2_weighted
    challengers = [Dao.randomPlayer, Dao.ai1]
    for challenger in challengers:
        print("=== Nouvelle partie ===")
        print(IA.__name__, "vs", challenger.__name__)
        print()
        winner = current_play.play_game(IA, challenger)
        if winner == 1:
            print(IA.__name__, "a gagné")
        elif winner == 2:
            print(challenger.__name__, "a gagné")
        print()


def multi_game_alphabeta():
    print()
    print("== multi game try ALPHA-BETA==")
    print()
    current_play = Dao()
    IA = Dao.ai2_weighted_alphabeta
    challengers = [Dao.randomPlayer, Dao.ai1_alphabeta]
    for challenger in challengers:
        print()
        print("=== Nouvelle partie ===")
        print(IA.__name__, "vs", challenger.__name__)
        winner = current_play.play_game(IA, challenger)
        if winner == 1:
            print(IA.__name__, "a gagné")
        elif winner == 2:
            print(challenger.__name__, "a gagné")
        print()


def scoring():
    print()
    print("== scoring ALPHA-BETA==")
    print()
    current_play = Dao()
    number_of_plays = 21
    scores = []
    challengers = [Dao.ai2_weighted_alphabeta, Dao.ai1_alphabeta]
    for challenger in challengers:
        score = 0.0
        wins = 0
        for i in range(number_of_plays):
            print("partie n", number_of_plays)
            winner = current_play.play_game(challenger, Dao.randomPlayer)
            if winner == 1:
                wins = wins + 1
        score = wins / number_of_plays
        scores.append(score)
    for i, challenger in enumerate(challengers):
        print(challenger.__name__, "a obtenu un score de",
              scores[i], "sur", number_of_plays, "parties")


random_move()
full_random()
one_move()
result_try()
manual_minmax()
one_try()
multi_game()
multi_game_alphabeta()
scoring()
